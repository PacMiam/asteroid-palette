Asteroid Palette
================

A little 16 colors palette for my GNU/Linux environment :D

![Preview](preview.png)

Available colors
----------------

### Light variant

| Name          | Hexadecimal | RGB                |
| ------------- | ----------- | ------------------ |
| Light red     | #FF6666     | rgb(255, 102, 102) |
| Light orange  | #FFAF66     | rgb(255, 175, 102) |
| Light yellow  | #FFE280     | rgb(255, 226, 128) |
| Light green   | #9CFF80     | rgb(156, 255, 128) |
| Light cyan    | #66FFDA     | rgb(102, 255, 218) |
| Light blue    | #66F3FF     | rgb(102, 243, 255) |
| Light magenta | #FFB3FB     | rgb(255, 179, 251) |
| Light maroon  | #FFCCB3     | rgb(255, 204, 179) |

### Dark variant

| Name          | Hexadecimal | RGB                |
| ------------- | ----------- | ------------------ |
| Dark red      | #E65C5C     | rgb(230, 92, 92)   |
| Dark orange   | #E69D5C     | rgb(230, 157, 92)  |
| Dark yellow   | #E6CB73     | rgb(230, 203, 115) |
| Dark green    | #8CE673     | rgb(140, 230, 115) |
| Dark cyan     | #5CE6C4     | rgb(92, 230, 196)  |
| Dark blue     | #5CDBE6     | rgb(92, 219, 230)  |
| Dark magenta  | #E6A1E2     | rgb(230, 161, 226) |
| Dark maroon   | #E6B8A1     | rgb(230, 184, 161) |

### Other colors

| Name          | Hexadecimal | RGB                |
| ------------- | ----------- | ------------------ |
| Black         | #1A1A1A     | rgb(26, 26, 26)    |
| Grey 000      | #262626     | rgb(38, 38, 38)    |
| Grey 001      | #333333     | rgb(51, 51, 51)    |
| Grey 010      | #4D4D4D     | rgb(77, 77, 77)    |
| Grey 011      | #666666     | rgb(102, 102, 102) |
| Grey 100      | #999999     | rgb(153, 153, 153) |
| Grey 101      | #CCCCCC     | rgb(204, 204, 204) |
| White         | #E2E2E2     | rgb(226, 226, 226) |
