Asteroid Palette
================

GIMP
----

Copy asteroid.gpl to GIMP configuration folder :

```
$ cp asteroid.gpl $HOME/.config/GIMP/2.10/palettes/
```

Inkscape
--------

Copy asteroid.gpl to Inkscape configuration folder :

```
$ cp asteroid.gpl $HOME/.config/inkscape/palettes/
```
